/**
 * AsyncComponent
 * Code Splitting Component / Server Side Rendering
 */
import React from "react";
import Loadable from "react-loadable";

// rct page loader
import RctPageLoader from "components/RctPageLoader/RctPageLoader";
import PreloadTable from "components/PreloadLayout/PreloadTable";
import PreloadProduct from "components/PreloadLayout/preloadProduct";

// ecommerce dashboard
const AsyncEcommerceDashboardComponent = Loadable({
  loader: () => import("routes/dashboard"),
  loading: () => <RctPageLoader />,
});
const AsyncCustomersListComponent = Loadable({
  loader: () => import("routes/customers/CustomersList"),
  loading: () => <PreloadTable />,
});
const AsyncCustomerViewComponent = Loadable({
  loader: () => import("routes/customers/ViewCustomer"),
  loading: () => <RctPageLoader />,
});
const AsyncCustomerDetailComponent = Loadable({
  loader: () => import("routes/customers/EditCustomer"),
  loading: () => <RctPageLoader />,
});

export {
  AsyncEcommerceDashboardComponent,
  AsyncCustomersListComponent,
  AsyncCustomerViewComponent,
  AsyncCustomerDetailComponent,
};
