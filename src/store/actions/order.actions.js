import Domain from "lib/Config";
import axios from "axios";
import { NotificationManager } from "react-notifications";
export const GET_ORDERS_ADMIN = "[ADMIN] GET_ORDERS_ADMIN";
export const ADMIN_ORDER_LOADING = "[ADMIN] ADMIN_ORDER_LOADING";
export const ADMIN_ORDER_ERROR = "[ADMIN] ADMIN_ORDER_ERROR";
export const ORDERS_DETAIL_ADMIN = "[ADMIN] ORDERS_DETAIL_ADMIN";
export const ORDER_DETAIL_LOADING = "[ADMIN] ORDER_DETAIL_LOADING";
export const ADMIN_UPDATE_ORDER = "[ADMIN] ADMIN_UPDATE_ORDER";
export const ORDERS_COUNT_GRAPH = "[ADMIN] ORDERS_COUNT_GRAPH";
export const LOADING_COUNT_GRAPH = "[ADMIN] LOADING_COUNT_GRAPH";
export const GET_TAX_DATA = "[ADMIN] GET_TAX_DATA";
export const TAX_LOADING = "[ADMIN] TAX_LOADING";
export const EDIT_TAX = "[ADMIN] EDIT_TAX";

export function getAdminTaxData(variables) {
  const request = axios.post(`${Domain}/api/admin/getTaxData`, { variables });

  return (dispatch) =>
    request
      .then((response) => {
        return dispatch({
          type: GET_TAX_DATA,
          payload: response.data.result,
        });
      })
      .catch((error) => {
        error.response
          ? NotificationManager.error(error.response.data.msg)
          : NotificationManager.error("Error! Cannot fetch tax data");
      });
}
export function adminTaxLoading(val) {
  return (dispatch) => {
    return dispatch({
      type: TAX_LOADING,
      payload: val,
    });
  };
}
export function framingOrdersAdmin(page, variables) {
  const request = axios.post(
    `${Domain}/api/admin/framingOrdersAdmin`,
    { variables },
    {
      params: { page },
    }
  );

  return (dispatch) =>
    request
      .then((response) => {
        return dispatch({
          type: GET_ORDERS_ADMIN,
          payload: response.data.result,
        });
      })
      .catch((error) => {
        return dispatch({
          type: ADMIN_ORDER_ERROR,
          payload:
            error.response && error.response.data.msg
              ? error.response.data.msg
              : "Error! Cannot get orders",
        });
      });
}
export function editFramingOrderAdmin(data) {
  const request = axios.put(`${Domain}/api/admin/editFramingOrderAdmin`, data);

  return (dispatch) =>
    request
      .then((response) => {
        return dispatch({
          type: ADMIN_UPDATE_ORDER,
        });
      })
      .then(() => dispatch(orderDetailAdmin(data.id)))
      .then(() => NotificationManager.success("Order updated successfully"))
      .catch((error) => {
        dispatch(OrderDetailLoading(false));
        error.response
          ? NotificationManager.error(error.response.data.msg)
          : NotificationManager.error("Error! Cannot update order");
      });
}
export function getOrdersAdmin(page, variables, customer_id, coupon_id) {
  const request = axios.post(
    `${Domain}/api/admin/getOrdersAdmin`,
    { variables, customer_id, coupon_id },
    {
      params: { page },
    }
  );

  return (dispatch) =>
    request
      .then((response) => {
        return dispatch({
          type: GET_ORDERS_ADMIN,
          payload: response.data.result,
        });
      })
      .catch((error) => {
        dispatch(adminOrderLoading(false));
        error.response
          ? NotificationManager.error(error.response.data.msg)
          : NotificationManager.error("Error! Cannot fetch orders");
      });
}

export function adminOrderLoading(val) {
  return (dispatch) => {
    return dispatch({
      type: ADMIN_ORDER_LOADING,
      payload: val,
    });
  };
}

export function orderDetailAdmin(id) {
  const request = axios.get(`${Domain}/api/admin/orderDetailAdmin`, {
    params: { id },
  });

  return (dispatch) =>
    request
      .then((response) => {
        return dispatch({
          type: ORDERS_DETAIL_ADMIN,
          payload: response.data.result,
        });
      })
      .catch((error) => {
        dispatch(OrderDetailLoading(false));
        error.response
          ? NotificationManager.error(error.response.data.msg)
          : NotificationManager.error("Error! Cannot fetch orders");
      });
}

export function OrderDetailLoading(val) {
  return (dispatch) => {
    return dispatch({
      type: ORDER_DETAIL_LOADING,
      payload: val,
    });
  };
}

export function adminUpdateOrder(data) {
  const request = axios.put(`${Domain}/api/admin/adminUpdateOrder`, data);

  return (dispatch) =>
    request
      .then((response) => {
        return dispatch({
          type: ADMIN_UPDATE_ORDER,
        });
      })
      .then(() => dispatch(orderDetailAdmin(data.id)))
      .catch((error) => {
        dispatch(OrderDetailLoading(false));
        error.response
          ? NotificationManager.error(error.response.data.msg)
          : NotificationManager.error("Error! Cannot update order");
      });
}
