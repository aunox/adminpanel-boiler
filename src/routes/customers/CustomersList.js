import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, FormGroup, Input, Button } from "reactstrap";
import ReactTable from "react-table-6";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ListIcon from "@material-ui/icons/List";
import DatePicker from "components/DatePicker/DatePicker";
import * as Actions from "store/actions";
import history from "@history";
import "react-table-6/react-table.css";
import "./tagStyles.css";
import qs from "query-string";
import moment from "moment";

function CustomersList(props) {
  let querystr = qs.parse(props.location.search);
  let cust_page = querystr.page ? JSON.parse(querystr.page) : 1;
  const dispatch = useDispatch();
  const [customers, setCustomers] = useState(null);
  const [variables, setVariables] = useState({ ...querystr });

  const CustomersList = useSelector(({ customers }) => customers);
  useEffect(() => {
    if (CustomersList) {
      const q_str = qs.stringify(variables);
      history.push({ search: q_str });
      dispatch(Actions.CustomerLoading(true));
      dispatch(Actions.adminGetCustomers(variables));
    }
  }, [dispatch, variables]);

  useEffect(() => {
    setCustomers(CustomersList);
  }, [CustomersList]);

  const handleDateChange = (e) => {
    if (!e) {
      delete variables.dateRange;
      setVariables({ ...variables, page: 1 });
      return;
    }
    const formatedDate = [
      moment(e[0]).format("MM-DD-YYYY"),
      moment(e[1]).format("MM-DD-YYYY"),
    ];
    setVariables({
      ...variables,
      dateRange: formatedDate,
      page: 1,
    });
  };

  const handlePageChange = (pg) => {
    setVariables({
      ...variables,
      page: pg + 1,
    });
  };
  const handleSearch = (e) => {
    e.preventDefault();
    let searchKey = `_${e.target.key.value}`;
    let searchVal = e.target.value.value;
    setVariables({ ...variables, [searchKey]: searchVal, page: 1 });
    document.getElementById("search-inp").value = "";
  };

  const handleTagDel = (id) => {
    delete variables[id];
    setVariables({ ...variables, page: 1 });
  };

  const handleSorting = (val) => {
    let sortingData = `${val[0].id}-${val[0].desc ? "DESC" : "ASC"}`;
    setVariables({
      ...variables,
      sorting: sortingData,
    });
  };
  const columns = [
    {
      maxWidth: 75,
      Header: "No.",
      accessor: "id",
    },
    {
      Header: "Name",
      accessor: "fullName",
    },
    {
      Header: "Email",
      accessor: "email",
    },
    {
      Header: "Phone",
      accessor: "phone",
    },
    {
      Header: "Status",
      accessor: "status",
      maxWidth: 100,
      Cell: (props) => (
        <div style={{ textAlign: "center" }}>
          {props.value ? (
            <span style={{ color: "#42ed2f", fontWeight: 500 }}>Active</span>
          ) : (
            <span style={{ color: "#FF3739", fontWeight: 500 }}>Banned</span>
          )}
        </div>
      ),
    },
    {
      Header: "Created At",
      accessor: "createdAt",
    },
    {
      Header: "View",
      maxWidth: 80,
      sortable: false,
      style: { textAlign: "center" },
      Cell: (props) => {
        return (
          <div>
            <IconButton
              color="inherit"
              mini="true"
              aria-label="Menu"
              className="p-0"
              onClick={() =>
                history.push({
                  pathname: `/admin/view-customer/${props.original.id}`,
                })
              }
            >
              <VisibilityIcon />
            </IconButton>
          </div>
        );
      },
    },
    {
      Header: "Orders",
      accessor: "orders",
      maxWidth: 80,
      sortable: false,
      style: {
        textAlign: "center",
      },
      Cell: (props) => {
        return (
          <div>
            <IconButton
              color="inherit"
              mini="true"
              aria-label="Menu"
              className="p-0"
              onClick={() =>
                history.push({
                  pathname: "/admin/orderslist",
                  search: `?customer_id=${props.original.id}`,
                })
              }
            >
              <ListIcon />
            </IconButton>
          </div>
        );
      },
    },
  ];
  if (customers && customers.error) {
    return (
      <div
        className="page-loader d-flex flex-column justify-content-center align-items-center"
        style={{ minHeight: "300px" }}
      >
        <h2>Error! Cannot fetch customers.</h2>
        <Button color="danger" className="mt-3" onClick={props.history.goBack}>
          Back
        </Button>
      </div>
    );
  }

  if (customers === null || !customers.customerList) {
    return (
      <div
        className="page-loader d-flex justify-content-center mb-30"
        style={{ height: "500px" }}
      >
        <CircularProgress style={{ margin: "auto" }} />
      </div>
    );
  }
  return (
    <div>
      <div className="page-title d-flex justify-content-between align-items-center">
        <div className="page-title-wrap">
          <i
            style={{ cursor: "pointer" }}
            onClick={props.history.goBack}
            className="ti-angle-left"
          ></i>
          <h2 className="">Customers List</h2>
        </div>
      </div>
      <div className="container">
        <div className="mb-3 d-flex justify-content-end flex-wrap">
          <div className="tag-container">
            {variables &&
              Object.keys(variables).map((sd, idx) =>
                sd.charAt(0) === "_" && variables[sd] ? (
                  <span className="tag-spans" key={idx}>
                    {variables[sd]}
                    <button
                      className="tag-del-btn"
                      onClick={() => handleTagDel(sd)}
                    >
                      <i className="zmdi zmdi-close"></i>
                    </button>
                  </span>
                ) : null
              )}
          </div>
        </div>
        <div className="mb-2 d-flex justify-content-end flex-wrap">
          <DatePicker
            handleChange={handleDateChange}
            value={variables.dateRange}
          />
          <Form className="mt-2 mt-sm-0" onSubmit={(e) => handleSearch(e)}>
            <FormGroup className="mx-2 d-inline-block gift-search-input">
              <Input type="select" name="key">
                <option value="id">id</option>
                <option value="fullName">Name</option>
                <option value="email">Email</option>
                <option value="phone">Phone</option>
              </Input>
            </FormGroup>
            <FormGroup className="d-inline-block gift-search-input">
              <Input
                type="text"
                name="value"
                id="search-inp"
                maxLength={10}
                placeholder="search query..."
                required
              />
              <button type="submit" className="gift-search-icon">
                <i
                  className="zmdi zmdi-search"
                  style={{ color: "#5D92F4" }}
                ></i>
              </button>
            </FormGroup>
          </Form>
        </div>
      </div>
      <div className="container">
        <ReactTable
          className="invoices-table"
          loading={customers.Loading}
          loadingText={<CircularProgress />}
          data={customers.customerList}
          columns={columns}
          onPageChange={(pg) => handlePageChange(pg)}
          onSortedChange={(newSorted) => handleSorting(newSorted)}
          page={cust_page - 1}
          manual
          pages={Math.ceil(customers.totalCustomers / 20)}
          showPageSizeOptions={false}
          showPageJump={true}
          minRows={1}
        />
      </div>
    </div>
  );
}

export default CustomersList;

