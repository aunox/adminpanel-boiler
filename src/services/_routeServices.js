// async component
import {
  AsyncEcommerceDashboardComponent,
  AsyncCustomersListComponent,
  AsyncCustomerDetailComponent,
  AsyncCustomerViewComponent,
} from "components/AsyncComponent/AsyncComponent";

export default [
  {
    path: "dashboard",
    component: AsyncEcommerceDashboardComponent,
  },
  {
    path: "customerslist",
    component: AsyncCustomersListComponent,
  },
  {
    path: "view-customer/:id",
    component: AsyncCustomerViewComponent,
  },
  {
    path: "edit-customer/:id",
    component: AsyncCustomerDetailComponent,
  },
];
